﻿namespace TimeBackUp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.textBox1 = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.btnStart = new MaterialSkin.Controls.MaterialRaisedButton();
            this.txtSavePath1 = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.BtnSavePath = new MaterialSkin.Controls.MaterialRaisedButton();
            this.BtnPlus = new MaterialSkin.Controls.MaterialRaisedButton();
            this.BtnPathDB = new MaterialSkin.Controls.MaterialRaisedButton();
            this.txtDBAddress = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.BtnLess = new MaterialSkin.Controls.MaterialRaisedButton();
            this.txtSavePath2 = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtSavePath3 = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtSavePath4 = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label7 = new System.Windows.Forms.Label();
            this.materialSingleLineTextField1 = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnSavePath2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.BtnSavePath3 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.BtnSavePath4 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.saveFileDialog2 = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(792, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "دوره زمانی";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(767, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "مسیر ذخیره سازی";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(755, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "مسیر بانک اطلاعاتی";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.BtnStart_Click);
            // 
            // textBox1
            // 
            this.textBox1.Depth = 0;
            this.textBox1.Hint = "";
            this.textBox1.Location = new System.Drawing.Point(679, 72);
            this.textBox1.MouseState = MaterialSkin.MouseState.HOVER;
            this.textBox1.Name = "textBox1";
            this.textBox1.PasswordChar = '\0';
            this.textBox1.SelectedText = "";
            this.textBox1.SelectionLength = 0;
            this.textBox1.SelectionStart = 0;
            this.textBox1.Size = new System.Drawing.Size(70, 23);
            this.textBox1.TabIndex = 24;
            this.textBox1.UseSystemPasswordChar = false;
            // 
            // btnStart
            // 
            this.btnStart.Depth = 0;
            this.btnStart.Location = new System.Drawing.Point(8, 72);
            this.btnStart.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnStart.Name = "btnStart";
            this.btnStart.Primary = true;
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 25;
            this.btnStart.Text = "بکاپ گیری";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // txtSavePath1
            // 
            this.txtSavePath1.Depth = 0;
            this.txtSavePath1.Enabled = false;
            this.txtSavePath1.Hint = "";
            this.txtSavePath1.Location = new System.Drawing.Point(115, 146);
            this.txtSavePath1.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSavePath1.Name = "txtSavePath1";
            this.txtSavePath1.PasswordChar = '\0';
            this.txtSavePath1.SelectedText = "";
            this.txtSavePath1.SelectionLength = 0;
            this.txtSavePath1.SelectionStart = 0;
            this.txtSavePath1.Size = new System.Drawing.Size(607, 23);
            this.txtSavePath1.TabIndex = 24;
            this.txtSavePath1.UseSystemPasswordChar = false;
            // 
            // BtnSavePath
            // 
            this.BtnSavePath.Depth = 0;
            this.BtnSavePath.Location = new System.Drawing.Point(8, 146);
            this.BtnSavePath.MouseState = MaterialSkin.MouseState.HOVER;
            this.BtnSavePath.Name = "BtnSavePath";
            this.BtnSavePath.Primary = true;
            this.BtnSavePath.Size = new System.Drawing.Size(75, 23);
            this.BtnSavePath.TabIndex = 25;
            this.BtnSavePath.Text = "......";
            this.BtnSavePath.UseVisualStyleBackColor = true;
            this.BtnSavePath.Click += new System.EventHandler(this.BtnSavePath_Click);
            // 
            // BtnPlus
            // 
            this.BtnPlus.Depth = 0;
            this.BtnPlus.Location = new System.Drawing.Point(721, 146);
            this.BtnPlus.MouseState = MaterialSkin.MouseState.HOVER;
            this.BtnPlus.Name = "BtnPlus";
            this.BtnPlus.Primary = true;
            this.BtnPlus.Size = new System.Drawing.Size(28, 23);
            this.BtnPlus.TabIndex = 26;
            this.BtnPlus.Text = "+";
            this.BtnPlus.UseVisualStyleBackColor = true;
            this.BtnPlus.Click += new System.EventHandler(this.BtnPlus_Click);
            // 
            // BtnPathDB
            // 
            this.BtnPathDB.Depth = 0;
            this.BtnPathDB.Location = new System.Drawing.Point(8, 107);
            this.BtnPathDB.MouseState = MaterialSkin.MouseState.HOVER;
            this.BtnPathDB.Name = "BtnPathDB";
            this.BtnPathDB.Primary = true;
            this.BtnPathDB.Size = new System.Drawing.Size(75, 23);
            this.BtnPathDB.TabIndex = 25;
            this.BtnPathDB.Text = "......";
            this.BtnPathDB.UseVisualStyleBackColor = true;
            this.BtnPathDB.Click += new System.EventHandler(this.BtnPathDB_Click);
            // 
            // txtDBAddress
            // 
            this.txtDBAddress.Depth = 0;
            this.txtDBAddress.Enabled = false;
            this.txtDBAddress.Hint = "";
            this.txtDBAddress.Location = new System.Drawing.Point(89, 107);
            this.txtDBAddress.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtDBAddress.Name = "txtDBAddress";
            this.txtDBAddress.PasswordChar = '\0';
            this.txtDBAddress.SelectedText = "";
            this.txtDBAddress.SelectionLength = 0;
            this.txtDBAddress.SelectionStart = 0;
            this.txtDBAddress.Size = new System.Drawing.Size(660, 23);
            this.txtDBAddress.TabIndex = 24;
            this.txtDBAddress.UseSystemPasswordChar = false;
            // 
            // BtnLess
            // 
            this.BtnLess.Depth = 0;
            this.BtnLess.Location = new System.Drawing.Point(89, 146);
            this.BtnLess.MouseState = MaterialSkin.MouseState.HOVER;
            this.BtnLess.Name = "BtnLess";
            this.BtnLess.Primary = true;
            this.BtnLess.Size = new System.Drawing.Size(28, 23);
            this.BtnLess.TabIndex = 26;
            this.BtnLess.Text = "-";
            this.BtnLess.UseVisualStyleBackColor = true;
            this.BtnLess.Click += new System.EventHandler(this.BtnLess_Click);
            // 
            // txtSavePath2
            // 
            this.txtSavePath2.Depth = 0;
            this.txtSavePath2.Enabled = false;
            this.txtSavePath2.Hint = "";
            this.txtSavePath2.Location = new System.Drawing.Point(115, 180);
            this.txtSavePath2.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSavePath2.Name = "txtSavePath2";
            this.txtSavePath2.PasswordChar = '\0';
            this.txtSavePath2.SelectedText = "";
            this.txtSavePath2.SelectionLength = 0;
            this.txtSavePath2.SelectionStart = 0;
            this.txtSavePath2.Size = new System.Drawing.Size(607, 23);
            this.txtSavePath2.TabIndex = 24;
            this.txtSavePath2.UseSystemPasswordChar = false;
            this.txtSavePath2.Visible = false;
            // 
            // txtSavePath3
            // 
            this.txtSavePath3.Depth = 0;
            this.txtSavePath3.Enabled = false;
            this.txtSavePath3.Hint = "";
            this.txtSavePath3.Location = new System.Drawing.Point(115, 209);
            this.txtSavePath3.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSavePath3.Name = "txtSavePath3";
            this.txtSavePath3.PasswordChar = '\0';
            this.txtSavePath3.SelectedText = "";
            this.txtSavePath3.SelectionLength = 0;
            this.txtSavePath3.SelectionStart = 0;
            this.txtSavePath3.Size = new System.Drawing.Size(607, 23);
            this.txtSavePath3.TabIndex = 24;
            this.txtSavePath3.UseSystemPasswordChar = false;
            this.txtSavePath3.Visible = false;
            // 
            // txtSavePath4
            // 
            this.txtSavePath4.Depth = 0;
            this.txtSavePath4.Enabled = false;
            this.txtSavePath4.Hint = "";
            this.txtSavePath4.Location = new System.Drawing.Point(115, 238);
            this.txtSavePath4.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSavePath4.Name = "txtSavePath4";
            this.txtSavePath4.PasswordChar = '\0';
            this.txtSavePath4.SelectedText = "";
            this.txtSavePath4.SelectionLength = 0;
            this.txtSavePath4.SelectionStart = 0;
            this.txtSavePath4.Size = new System.Drawing.Size(607, 23);
            this.txtSavePath4.TabIndex = 24;
            this.txtSavePath4.UseSystemPasswordChar = false;
            this.txtSavePath4.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(532, 78);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(124, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "تعداد بکاپ های مورد نیاز";
            // 
            // materialSingleLineTextField1
            // 
            this.materialSingleLineTextField1.Depth = 0;
            this.materialSingleLineTextField1.Hint = "";
            this.materialSingleLineTextField1.Location = new System.Drawing.Point(440, 72);
            this.materialSingleLineTextField1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialSingleLineTextField1.Name = "materialSingleLineTextField1";
            this.materialSingleLineTextField1.PasswordChar = '\0';
            this.materialSingleLineTextField1.SelectedText = "";
            this.materialSingleLineTextField1.SelectionLength = 0;
            this.materialSingleLineTextField1.SelectionStart = 0;
            this.materialSingleLineTextField1.Size = new System.Drawing.Size(70, 23);
            this.materialSingleLineTextField1.TabIndex = 24;
            this.materialSingleLineTextField1.UseSystemPasswordChar = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(323, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "label1";
            // 
            // BtnSavePath2
            // 
            this.BtnSavePath2.Depth = 0;
            this.BtnSavePath2.Location = new System.Drawing.Point(8, 175);
            this.BtnSavePath2.MouseState = MaterialSkin.MouseState.HOVER;
            this.BtnSavePath2.Name = "BtnSavePath2";
            this.BtnSavePath2.Primary = true;
            this.BtnSavePath2.Size = new System.Drawing.Size(75, 23);
            this.BtnSavePath2.TabIndex = 25;
            this.BtnSavePath2.Text = "......";
            this.BtnSavePath2.UseVisualStyleBackColor = true;
            this.BtnSavePath2.Visible = false;
            this.BtnSavePath2.Click += new System.EventHandler(this.BtnSavePath2_Click_1);
            // 
            // BtnSavePath3
            // 
            this.BtnSavePath3.Depth = 0;
            this.BtnSavePath3.Location = new System.Drawing.Point(8, 204);
            this.BtnSavePath3.MouseState = MaterialSkin.MouseState.HOVER;
            this.BtnSavePath3.Name = "BtnSavePath3";
            this.BtnSavePath3.Primary = true;
            this.BtnSavePath3.Size = new System.Drawing.Size(75, 23);
            this.BtnSavePath3.TabIndex = 25;
            this.BtnSavePath3.Text = "......";
            this.BtnSavePath3.UseVisualStyleBackColor = true;
            this.BtnSavePath3.Visible = false;
            this.BtnSavePath3.Click += new System.EventHandler(this.BtnSavePath_Click);
            // 
            // BtnSavePath4
            // 
            this.BtnSavePath4.Depth = 0;
            this.BtnSavePath4.Location = new System.Drawing.Point(8, 238);
            this.BtnSavePath4.MouseState = MaterialSkin.MouseState.HOVER;
            this.BtnSavePath4.Name = "BtnSavePath4";
            this.BtnSavePath4.Primary = true;
            this.BtnSavePath4.Size = new System.Drawing.Size(75, 23);
            this.BtnSavePath4.TabIndex = 25;
            this.BtnSavePath4.Text = "......";
            this.BtnSavePath4.UseVisualStyleBackColor = true;
            this.BtnSavePath4.Visible = false;
            this.BtnSavePath4.Click += new System.EventHandler(this.BtnSavePath_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(864, 386);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnLess);
            this.Controls.Add(this.BtnPlus);
            this.Controls.Add(this.BtnPathDB);
            this.Controls.Add(this.BtnSavePath4);
            this.Controls.Add(this.BtnSavePath3);
            this.Controls.Add(this.BtnSavePath2);
            this.Controls.Add(this.BtnSavePath);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.txtDBAddress);
            this.Controls.Add(this.txtSavePath4);
            this.Controls.Add(this.txtSavePath3);
            this.Controls.Add(this.txtSavePath2);
            this.Controls.Add(this.txtSavePath1);
            this.Controls.Add(this.materialSingleLineTextField1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Timer timer1;
        private MaterialSkin.Controls.MaterialSingleLineTextField textBox1;
        private MaterialSkin.Controls.MaterialRaisedButton btnStart;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSavePath1;
        private MaterialSkin.Controls.MaterialRaisedButton BtnSavePath;
        private MaterialSkin.Controls.MaterialRaisedButton BtnPlus;
        private MaterialSkin.Controls.MaterialRaisedButton BtnPathDB;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtDBAddress;
        private MaterialSkin.Controls.MaterialRaisedButton BtnLess;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSavePath2;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSavePath3;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSavePath4;
        private System.Windows.Forms.Label label7;
        private MaterialSkin.Controls.MaterialSingleLineTextField materialSingleLineTextField1;
        private System.Windows.Forms.Label label1;
        private MaterialSkin.Controls.MaterialRaisedButton BtnSavePath2;
        private MaterialSkin.Controls.MaterialRaisedButton BtnSavePath3;
        private MaterialSkin.Controls.MaterialRaisedButton BtnSavePath4;
        private System.Windows.Forms.SaveFileDialog saveFileDialog2;
    }
}

