﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Data.SqlClient;
using System.Timers;
using System.Windows.Forms;
using Timer = System.Timers.Timer;

namespace TimeBackUp
{
    public partial class Form1 : MaterialForm
    {
        public Form1()
        {
            InitializeComponent();
            var skinManager = MaterialSkinManager.Instance;
            skinManager.AddFormToManage(this);
            skinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            skinManager.ColorScheme = new ColorScheme(Primary.Blue800,
                Primary.Blue800, Primary.Purple700, Accent.Orange100,
                TextShade.WHITE);
        }

        private static Timer _timer;

        //private readonly DispatcherTimer _timer = new DispatcherTimer();
        private int _count = 1;
        private readonly string[] _addressStrings = new string[2];

        private bool Validation()
        {
            if (string.IsNullOrEmpty(txtDBAddress.Text))
            {
                MessageBox.Show("مسیر بانک اطلاعاتی را تعیین کنید");
                return true;
            }

            if (string.IsNullOrEmpty(txtSavePath1.Text))
            {
                MessageBox.Show("مسیر ذخیره را تعیین کنید");
                return true;
            }

            return false;
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            if (!Validation())
            {
                _timer = new Timer(double.Parse(textBox1.Text));

                _timer.Elapsed += BackUp;

                _timer.AutoReset = true;
                _timer.Enabled = true;
                // System.Threading.Timer threaTimer = new Timer(BackUp(), 10, 1, 3000);
                // timer1.Start();
                //this.Text = count++.ToString();
                //if (count == int.Parse(textBox1.Text))
                //    BackUp();
                //var s = DateTime.Now.TimeOfDay.ToString("g");
                //label1.Text = s.Substring(0, s.IndexOf("/"));
            }
        }


        private string TimeOfSaveFile()
        {
            var currentDateTime = DateTime.Now;
            var dateStr = currentDateTime.ToString("yyyy-MM-dd  HH_mm_ss");
            saveFileDialog1.FileName = dateStr;
            return dateStr;
        }

        private void BtnSavePath_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDBAddress.Text))
            {
                MessageBox.Show("مسیر بانک اطلاعاتی را تعیین کنید");
                return;
            }

            saveFileDialog1.Filter = @"BackupFile(*.Bak)|*.Bak";
            // saveFileDialog1.FileName = DateTime.Now.ToLongDateString();
            var nameFile = txtDBAddress.Text.Split('\\');
            var length = nameFile.Length - 1;
            label1.Text = nameFile[length];
            saveFileDialog1.FileName = nameFile[length] + "   " + TimeOfSaveFile();

            // _addressStrings[0] = txtSavePath1.Text;
            saveFileDialog1.ShowDialog();
            txtSavePath1.Text = saveFileDialog1.FileName;
            label1.Text = txtDBAddress.Text;
        }

        private void BtnSavePath2_Click_1(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDBAddress.Text))
            {
                MessageBox.Show(@"مسیر بانک اطلاعاتی را تعیین کنید");
                return;
            }

            saveFileDialog1.Filter = @"BackupFile(*.Bak)|*.Bak";
            // saveFileDialog1.FileName = DateTime.Now.ToLongDateString();
            var nameFile = txtDBAddress.Text.Split('\\');
            var length = nameFile.Length - 1;
            label1.Text = nameFile[length];
            saveFileDialog2.FileName = nameFile[length] + "   " + TimeOfSaveFile();
            txtSavePath2.Text = saveFileDialog2.FileName;
            _addressStrings[1] = txtSavePath2.Text;
            saveFileDialog2.ShowDialog();
        }

        private void BtnPathDB_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = @"DataBase(*.mdf)|*.mdf";
            openFileDialog1.ShowDialog();
            txtDBAddress.Text = openFileDialog1.FileName;
        }

        private void BackUp(object sender, ElapsedEventArgs e)
        {
            try
            {
                var cm = new SqlCommand
                {
                    Connection =
                        new SqlConnection(
                            $@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='{txtDBAddress.Text}';Integrated Security=True;Connect Timeout=30")
                };

                cm.Connection.Open();
                //SqlConnectionStringBuilder connBuilder = new SqlConnectionStringBuilder();
                //string database = connBuilder.InitialCatalog;
                //MessageBox.Show(database);

                var dbname = cm.Connection.Database;
                cm.Connection.Close();
                SqlConnection.ClearAllPools();
                // TimeOfSaveFile();

                var query =
                    $@"BACKUP DATABASE [{txtDBAddress.Text}] TO DISK ='" +
                    saveFileDialog1.FileName + "'";

                cm.Connection =
                    new SqlConnection(
                        $@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='{txtDBAddress.Text}';Integrated Security=True;Connect Timeout=30");
                cm.Connection.Open();
                cm.CommandText = query;
                cm.ExecuteNonQuery();
                cm.Connection.Close();
                TimeOfSaveFile();
                MessageBox.Show($"{saveFileDialog1.FileName}عملیات بکاپ گیری با موفقیت انجام شد");

            }
            catch (Exception exception)
            {
                MessageBox.Show("خطا در بکاپ گیری" + exception);
            }
        }

        private void BackUp2(object sender, ElapsedEventArgs e)
        {
            try
            {
                var cm = new SqlCommand
                {
                    Connection =
                        new SqlConnection(
                            $@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='{txtDBAddress.Text}';Integrated Security=True;Connect Timeout=30")
                };

                cm.Connection.Open();
                //SqlConnectionStringBuilder connBuilder = new SqlConnectionStringBuilder();
                //string database = connBuilder.InitialCatalog;
                //MessageBox.Show(database);

                var dbname = cm.Connection.Database;
                cm.Connection.Close();
                SqlConnection.ClearAllPools();
                // TimeOfSaveFile();
                var query =
                    $@"BACKUP DATABASE [{txtDBAddress.Text}] TO DISK ='" +
                    saveFileDialog2.FileName + "'";
                cm.Connection =
                    new SqlConnection(
                        $@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='{txtDBAddress.Text}';Integrated Security=True;Connect Timeout=30");
                cm.Connection.Open();
                cm.CommandText = query;
                cm.ExecuteNonQuery();
                cm.Connection.Close();
                TimeOfSaveFile();
                MessageBox.Show($"{saveFileDialog2.FileName}عملیات بکاپ گیری با موفقیت انجام شد");
            }
            catch (Exception exception)
            {
                MessageBox.Show("خطا در بکاپ گیری" + exception);
            }
        }

        private void BtnPlus_Click(object sender, EventArgs e)
        {
            _count++;
            // string txtname = txtSavePath2.Name + count;
            label1.Text = _count.ToString();
            switch (_count)
            {
                case 2:
                    txtSavePath2.Visible = true;
                    BtnSavePath2.Visible = true;
                    break;

                case 3:
                    if (txtSavePath2.Visible == false)
                    {
                        _count = 2;
                        txtSavePath2.Visible = true;
                        BtnSavePath2.Visible = true;
                    }
                    else
                    {
                        txtSavePath3.Visible = true;
                        BtnSavePath3.Visible = true;
                    }

                    break;
            }
            //if (txtname == "txtSavePath22")
            //{

            //}
            //string txtname3 = txtSavePath3.Name + count;

            //if (txtname3 == "txtSavePath33")
            //{
            //    txtSavePath3.Visible = true;
            //}
            //string txtname4 = txtSavePath4.Name + count;

            //if (txtname4 == "txtSavePath44")
            //{
            //    txtSavePath4.Visible = true;
            //}
        }

        private void BtnLess_Click(object sender, EventArgs e)
        {
            label1.Text = _count.ToString();
            // string txtname = txtSavePath2.Name + count;
            switch (_count)
            {
                case 2:

                    txtSavePath2.Visible = false;
                    BtnSavePath2.Visible = false;


                    break;
                case 3:
                    if (txtSavePath2.Visible == false)
                    {
                        _count = 2;
                        txtSavePath2.Visible = false;
                        BtnSavePath2.Visible = false;
                    }
                    else
                    {
                        txtSavePath3.Visible = false;

                        BtnSavePath3.Visible = false;
                        _count = 2;
                    }

                    break;
            }
        }
    }
}